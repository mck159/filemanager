package com.example.filemanager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.filemanager.fileUtils.SortType;

public class MainActivity extends Activity {
	static enum IconSize {
		SMALL, MEDIUM, LARGE
	}

	static enum ViewType {
		LIST, ICON
	}

	static enum ActionType {
		COPY, CUT, NONE
	}

	ViewType viewType = ViewType.ICON;
	SortType sortType = SortType.NAME;
	IconSize iconSize = IconSize.MEDIUM;
	boolean showHidden;

	ActionType action = ActionType.NONE;
	TextView textTitle;
	TextView textEmpty;
	ProgressBar progressBar;
	View viewFiles;
	IconArrayAdapter adapterFiles;
	ArrayList<File> fileList;
	ArrayList<File> checkedFilesList;
	File currentDir;
	ActionMode mActionMode;
	boolean show_selection = false;
	int itemId;
	int contentViewId;
	DialogInterface.OnClickListener dialogBackKeyListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int option) {
			if (option == DialogInterface.BUTTON_POSITIVE)
				finish();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fileList = new ArrayList<File>();
		checkedFilesList = new ArrayList<File>();
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadSettings(); // PLACE IN SOME MORE EFFECTIVE PLACE
		setView();
	}

	private void loadSettings() {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		String view = preferences.getString("pref_view", null);
		String sort = preferences.getString("pref_sort", null);
		String size = preferences.getString("pref_icon_size", null);
		showHidden = preferences.getBoolean("pref_show_hidden", true);

		for (ViewType v : ViewType.values())
			if (v.toString().equalsIgnoreCase(view))
				viewType = v;
		for (SortType s : SortType.values())
			if (s.toString().equalsIgnoreCase(sort))
				sortType = s;
		for (IconSize s : IconSize.values())
			if (s.toString().equalsIgnoreCase(size))
				iconSize = s;
	}

	private void saveSettings() {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("pref_view", viewType.toString());
		editor.putString("pref_sort", sortType.toString());
		editor.putString("pref_icon_size", iconSize.toString());
		editor.putBoolean("pref_show_hidden", showHidden);
		editor.commit();
	}

	protected void onDestroy() {
		saveSettings();
		super.onDestroy();
	}

	private class DefaultListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int position,
				long arg3) {
			changeDir(adapterFiles.getItem(position));
		}
	}

	DefaultListener defaultListener = new DefaultListener();

	private class SelectListener implements OnItemClickListener {
		public void onItemClick(AdapterView<?> arg0, View v, int position,
				long arg3) {
			File file = adapterFiles.getItem(position);
			if (!checkedFilesList.contains(file)) {
				checkedFilesList.add(file);
				v.setBackgroundColor(Color.RED);
			} else {
				checkedFilesList.remove(file);
				v.setBackground(null);
			}
		}
	}

	SelectListener selectListener = new SelectListener();

	private void setView() {
		if (viewType.equals(ViewType.LIST)) {
			contentViewId = R.layout.activity_main;
			itemId = R.layout.listview;
			setContentView(contentViewId);
			viewFiles = findViewById(R.id.listFiles);
		} else if (viewType.equals(ViewType.ICON)) {
			contentViewId = R.layout.activity_main_icon;
			setContentView(contentViewId);
			itemId = R.layout.listview_icon;
			viewFiles = findViewById(R.id.gridFiles);
			switch (iconSize) {
			// TODO: CHANGE TO SOME RELATIVE VALUES,
			// DEPENDING ON SCREEN SIZE
			case LARGE:
				((GridView) viewFiles).setNumColumns(3);
				break;
			case MEDIUM:
				((GridView) viewFiles).setNumColumns(4);
				break;
			case SMALL:
				((GridView) viewFiles).setNumColumns(5);
				break;
			default:
				break;
			}
		} else {
			return;
		}
		textTitle = (TextView) findViewById(R.id.textTitle);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		textEmpty = (TextView) findViewById(R.id.textEmpty);
		adapterFiles = new IconArrayAdapter(this, fileList);
		((AbsListView) viewFiles).setAdapter(adapterFiles);
		((AbsListView) viewFiles).setOnItemClickListener(defaultListener);

		changeDir(currentDir == null ? Environment
				.getExternalStorageDirectory() : currentDir);

		registerForContextMenu(viewFiles);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();

		switch (item.getItemId()) {
		case R.id.action_info:
			Intent i = new Intent(this, FileInfoActivity.class);
			i.putExtra("file", fileList.get(info.position).getAbsolutePath());
			startActivity(i);
			return true;
		case R.id.action_remove:
			new AlertDialog.Builder(this)
					.setPositiveButton("Yes", new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							fileUtils.remove(fileList.get(info.position));
							changeDir(currentDir);
						}
					}).setNegativeButton("No", null)
					.setTitle("Do you really want to delete this file?").show();
			return true;
		case R.id.action_copy:
			action = ActionType.COPY;
			checkedFilesList = new ArrayList<File>();
			checkedFilesList.add(fileList.get(info.position));
			return true;
		case R.id.action_cut:
			action = ActionType.CUT;
			checkedFilesList = new ArrayList<File>();
			checkedFilesList.add(fileList.get(info.position));
			return true;
		case R.id.action_rename:
			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainActivity.this);
			LayoutInflater inflater = getLayoutInflater();
			final View view;
			builder.setView(view = inflater.inflate(R.layout.dialog_rename,
					null));
			EditText editText = (EditText) view.findViewById(R.id.newFileName);
			editText.setText(fileList.get(info.position).getName());
			builder.setPositiveButton(R.string.rename,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							EditText editText = (EditText) view
									.findViewById(R.id.newFileName);
							if (!fileList.get(info.position).renameTo(
									new File(currentDir.getAbsolutePath() + "/"
											+ editText.getText().toString()))) {
								simpleToast("File not renamed", true);
							} else {
								changeDir(currentDir);
							}

						}
					})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							}).show();

			return true;
		default:
			return super.onContextItemSelected(item);

		}
	}

	@Override
	public void onBackPressed() {
		if (!dirUp()) {
			new AlertDialog.Builder(this)
					.setMessage(R.string.exit_confirmation_message)
					.setPositiveButton("Yes", dialogBackKeyListener)
					.setNegativeButton("No", dialogBackKeyListener).show();
		}
	}

	private void changeDir(File file) {
		if (file.isDirectory()) {
			listFiles(file, showHidden);
			currentDir = file;
			textTitle.setText(file.getAbsolutePath());
		} else {
			openFile(file);
		}// */
	}

	private boolean dirUp() {
		File parentFile = currentDir.getParentFile();

		if (parentFile == null)
			return false;

		changeDir(currentDir.getParentFile());
		return true;
	}

	private void openFile(File file) {
		Uri uri = Uri.fromFile(file);

		String mimeType = URLConnection
				.guessContentTypeFromName(uri.toString());

		mimeType = (mimeType == null) ? "text/plain" : mimeType;

		Intent target = new Intent(Intent.ACTION_VIEW);
		target.setDataAndType(uri, mimeType);
		try {
			startActivity(target);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(getApplicationContext(),
					"No application to open this type of file",
					Toast.LENGTH_SHORT).show();
		}
	}

	private void listFiles(File file, boolean showHidden) {
		fileList.clear();
		List<File> files = new ArrayList<File>();

		if (file.listFiles() != null)
			for (File f : file.listFiles())
				if (showHidden)
					files.add(f);
				else if (!f.isHidden())
					files.add(f);

		if (files.size() != 0) {
			fileList.addAll(files);
			fileUtils.sort(fileList, sortType);
			textEmpty.setVisibility(TextView.INVISIBLE);
		} else
			textEmpty.setVisibility(TextView.VISIBLE);

		adapterFiles.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		searchView.setIconified(false);
		searchView.clearFocus();
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				new searchThread().execute(query);
				return true;
			}

		});
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.selction_onoff:
			show_selection = show_selection ? false : true;
			if (show_selection) {
				((AbsListView) viewFiles)
						.setOnItemClickListener(selectListener);
			} else {
				((AbsListView) viewFiles)
						.setOnItemClickListener(defaultListener);
			}
			mActionMode = startActionMode(mActionModeCallback);
			adapterFiles.notifyDataSetChanged();
			checkedFilesList = new ArrayList<File>();
			return true;
		case R.id.action_toggle_view:
			if (viewType.equals(ViewType.LIST)) {
				viewType = ViewType.ICON;
				setView();
				item.setIcon(R.drawable.ic_action_view_as_list);
			} else if (viewType.equals(ViewType.ICON)) {
				viewType = ViewType.LIST;
				setView();
				item.setIcon(R.drawable.ic_action_view_as_grid);
			}
			return true;
		case R.id.action_sort_name:
			if (sortType.equals(SortType.NAME)) {
				fileUtils.reverse(fileList);
				sortType = SortType.NAME_R;
			} else {
				sortType = SortType.DATE;
				fileUtils.sort(fileList, sortType);
			}
			adapterFiles.notifyDataSetChanged();
			return true;
		case R.id.action_sort_date:
			if (sortType.equals(SortType.DATE)) {
				fileUtils.reverse(fileList);
				sortType = SortType.DATE_R;
			} else {
				sortType = SortType.DATE;
				fileUtils.sort(fileList, sortType);
			}
			adapterFiles.notifyDataSetChanged();
			return true;
		case R.id.action_sort_size:
			if (sortType.equals(SortType.SIZE)) {
				fileUtils.reverse(fileList);
				sortType = SortType.SIZE_R;
			} else {
				sortType = SortType.SIZE;
				fileUtils.sort(fileList, sortType);
			}
			adapterFiles.notifyDataSetChanged();
			return true;
		case R.id.action_sort_type:
			if (sortType.equals(SortType.TYPE)) {
				fileUtils.reverse(fileList);
				sortType = SortType.TYPE_R;
			} else {
				sortType = SortType.TYPE;
				fileUtils.sort(fileList, sortType);
			}
			adapterFiles.notifyDataSetChanged();
			return true;
		case R.id.action_paste:
			switch (action) {
			case CUT:
				try {
					fileUtils.cut(checkedFilesList, currentDir);
				} catch (IOException e) {
					simpleToast(e.getMessage(), true);
				}
				changeDir(currentDir);
				return true;
			case COPY:
				try {
					fileUtils.copy(checkedFilesList, currentDir);
				} catch (IOException e) {
					simpleToast(e.getMessage(), true);
				}
				changeDir(currentDir);
				return true;
			default:
				return false;
			}
		case R.id.action_settings:
			Intent i = new Intent(this, SettingsActivity.class);
			startActivity(i);
			return true;
		case R.id.action_about:
			InputStream in = getResources().openRawResource(R.raw.licence);
			java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
			String licence = s.hasNext() ? s.next() : "";
			s.close();
			new AlertDialog.Builder(this).setTitle(getString(R.string.about))
					.setMessage(licence).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void simpleToast(String text, boolean longer) {
		if (longer)
			Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG)
					.show();
		else
			Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT)
					.show();
	}

	@SuppressWarnings("unused")
	private void simpleToast(String text) {
		simpleToast(text, false);
	}

	class IconArrayAdapter extends ArrayAdapter<File> {
		private final Context context;
		private final ArrayList<File> values;

		public IconArrayAdapter(Context context, ArrayList<File> fileList) {
			super(context, itemId, fileList);
			this.context = context;
			this.values = fileList;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(itemId, parent, false);
			TextView textView = (TextView) rowView.findViewById(R.id.label);
			ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
			textView.setText(values.get(position).getName());
			textView.setLines(2);
			// Change icon based on name
			String type = fileUtils.getFileType(values.get(position));
			if (type.equals("directory"))
				textView.setTextColor(Color.BLUE);
			else
				textView.setTextColor(Color.BLACK);
			imageView.setImageResource(fileUtils.getMimeIcon(type));
			if (viewType == ViewType.LIST) {
				ViewGroup.LayoutParams params = imageView.getLayoutParams();
				textView.setLines(1);
				switch (iconSize) {
				// TODO: CHANGE TO SOME RELATIVE VALUES,
				// DEPENDING ON SCREEN SIZE
				case LARGE:
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
					break;
				case MEDIUM:
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
					break;
				case SMALL:
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
					break;
				default:
					break;
				}
				textView.measure(0, 0);
				params.width = textView.getMeasuredHeight();

			}
			return rowView;
		}
	}

	private class searchThread extends AsyncTask<String, Void, Void> {
		ArrayList<File> results;

		@Override
		protected void onPreExecute() {
			progressBar.setVisibility(ProgressBar.VISIBLE);
		}

		@Override
		protected Void doInBackground(String... params) {
			fileUtils.clearSearchList();
			results = fileUtils.searchInDir(params[0], currentDir);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressBar.setVisibility(ProgressBar.INVISIBLE);

			if (results == null) {
				simpleToast("ERROR", false);
			}
			fileList.clear();
			fileList.addAll(results);
			adapterFiles.notifyDataSetChanged();
			textTitle.setText("Search results");
			Toast.makeText(getApplicationContext(),
					"Found " + Integer.toString(results.size()) + " files",
					Toast.LENGTH_SHORT).show();

		}

	}

	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.file_menu, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.action_copy:
				action = ActionType.COPY;
				mode.finish();
				return true;
			case R.id.action_cut:
				action = ActionType.CUT;
				mode.finish();
				return true;
			case R.id.action_remove:
				new AlertDialog.Builder(MainActivity.this)
						.setPositiveButton("Yes", new OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								if (!fileUtils.remove(checkedFilesList)) {
									simpleToast("Delete failed", false);
								}
								changeDir(currentDir);
								mode.finish();
							}
						}).setNegativeButton("No", null)
						.setTitle("Do you really want to delete this file?")
						.show();
				return true;
			default:
				return false;
			}
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mActionMode = null;
			show_selection = false;
			((AbsListView) viewFiles).setOnItemClickListener(defaultListener);
			adapterFiles.notifyDataSetChanged();
		}
	};
}
