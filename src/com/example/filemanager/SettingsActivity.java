package com.example.filemanager;

import android.os.Bundle;
import android.preference.PreferenceActivity;


public class SettingsActivity extends PreferenceActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// TODO probably leave this until supporting API <= 10
		addPreferencesFromResource(R.xml.settings);
	}
	
}
